# Rules Cyclopedia compendium

Rules Cyclopedia weapons, armors, items and spells - Open Game License;

Rules Cyclopedia is a trademark of Wizards of the Coast;

Buy the full rules [PDF](https://www.drivethrurpg.com/product/17171/DD-Rules-Cyclopedia-Basic?term=rules+cyclopedia)

The icons used are free to use - https://game-icons.net/

Contact for bugs or suggestions: phillgamboa@gmail.com

